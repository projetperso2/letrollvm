import NotImplemented from "@base/errors/NotImplemented";
import { Arguments, StubObjectRef } from "@base/Type";
import { StubClass } from "@stub/StubClass";

export default class ThreadLocalRandom extends StubClass {
	constructor() {
		super("java/util/concurrent/ThreadLocalRandom", "java/lang/Object")
	}

  public current(...args: Arguments[]): StubObjectRef{
    const [descriptor] = args
    if(descriptor.value == "()Ljava/util/concurrent/ThreadLocalRandom;"){
      return {
        className: "java/util/concurrent/ThreadLocalRandom",
        fields: [],
        stubClass: this,
        type: "ObjectRef"
      }
    }else{
      console.log(args)
      throw new NotImplemented("current not implemented")  
    }
  }


}