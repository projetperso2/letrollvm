import NotImplemented from "@base/errors/NotImplemented";
import { Arguments, ObjectRef, StubObjectRef } from "@base/Type";
import { StubClass } from "@stub/StubClass";

export default class StringBuilder extends StubClass {
	constructor() {
		super("java/lang/StringBuilder", "java/lang/Object")
	}

  /**
   * 
   * @param args 
   */
  public __new__(...args: Arguments[]): StubObjectRef{
    return {
			className: this.javaClassName,
			fields: {},
			stubClass: this,
			type: "ObjectRef",
		}
  } 

  /**
   * 
   * @param args 
   */
  public __init__(...args: Arguments[]): void{
    const [classArg, objectRefArg] = args
    const objectRef = objectRefArg.value as ObjectRef
    objectRef.fields["content"] = ""
  }

}