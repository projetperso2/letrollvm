import NotImplemented from "@base/errors/NotImplemented";
import { Arguments, StubObjectRef } from "@base/Type";
import { StubClass } from "@stub/StubClass";

export default class DecimalFormat extends StubClass {
	constructor() {
		super("java/text/DecimalFormat", "java/lang/Object")
	}

  /**
   * 
   * @param args 
   */
  public __new__(...args: Arguments[]): StubObjectRef{
    return {
			className: this.javaClassName,
			fields: {
				format: "",
			},
			stubClass: this,
			type: "ObjectRef",
		}
  } 

  /**
   * 
   * @param args 
   */
  public __init__(...args: Arguments[]): void{
    const [classArg, objectRefArg, formatArg] = args
    const klass = classArg.value as StubClass
    const stubObjectRef = objectRefArg.value as StubObjectRef
    const formatValue = formatArg.value as string

    stubObjectRef.fields["format"] = formatValue
  }

}